from argparse import ArgumentParser
import requests
import json

slack_url = "https://hooks.slack.com/services/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"

# setting up the arguments
parser = ArgumentParser(description="Server Alert Notification Program")
parser.add_argument('--event',
                    help='Specify the event that is prompting this action',
                    required=True)
parser.add_argument('--message',
                    help='Message to describe the event',
                    required=True)

# parse the arguments
args = parser.parse_args()

payload = {"text": args.message, "attachments": [{
            "fallback": "Required plain-text summary of the attachment.",
            "color": "warning",
            "fields": [
                {
                    "title": "Priority",
                    "value": "High",
                    "short": "false"
                }
            ]
        }]}
json_payload = json.dumps(payload)

r = requests.post(slack_url, data=json_payload, json=True)