# Server Notify #

small script to enable email notifications since Windows 2012 Server doesn't allow for email actions anymore in Task Scheduler

### Usage ###

`serverNotify.py --event 'Your event' --message 'This is what happened' --sender 'sender@something.com' --recipient 'recipient@something.com'`